//
//  main.m
//  Touch ID Example
//
//  Created by omar megdadi on 20/10/14.
//  Copyright (c) 2014 NSStudent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
