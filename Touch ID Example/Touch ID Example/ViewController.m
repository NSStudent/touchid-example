//
//  ViewController.m
//  Touch ID Example
//
//  Created by omar megdadi on 20/10/14.
//  Copyright (c) 2014 NSStudent. All rights reserved.
//

#import "ViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)authAction:(id)sender
{
    LAContext *myContext = [[LAContext alloc] init];
    NSError *authError = nil;
    NSString *myLocalizedReasonString =NSLocalizedString(@"TouchIDReason", nil);
    
    if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError]) {
        [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                            reply:^(BOOL success, NSError *error) {
                                if (success) {
                                    NSLog(NSLocalizedString(@"SuccessMessage", nil));
                                    // User authenticated successfully, take appropriate action
                                } else {
                                    NSLog(NSLocalizedString(@"errorMessage", nil));
                                    // User did not authenticate successfully, look at error and take appropriate action
                                }
                            }];
    } else {
        NSLog(NSLocalizedString(@"authErrorMessage", nil));
        // Could not evaluate policy; look at authError and present an appropriate message to user
    }
}

@end
