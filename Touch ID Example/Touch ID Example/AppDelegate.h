//
//  AppDelegate.h
//  Touch ID Example
//
//  Created by omar megdadi on 20/10/14.
//  Copyright (c) 2014 NSStudent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

